# Terra Bootstrap

Boot with the Arch Linux live environment.
When logged, run:

```sh
loadkeys fr-latin9
curl https://gitlab.com/ludovicdelfau/terra-boot/-/archive/master/.tar.gz \
		| tar --one-top-level=terra-boot --strip-components=1 -xzv
fdisk -l
terra-boot/bootstrap ${DISK}
```

During the interactive session, create at least one administrative user:

```sh
useradd -m -G wheel -s /usr/bin/zsh -c ${USERNAME} ${USERID}
passwd ${USERID}
```
